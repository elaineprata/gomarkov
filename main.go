package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {

	file, err := os.Open("file/file.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	markov := &Chain{preProcessor: SimplePreProcessor{}}
	// markov := &Chain{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		phrase := scanner.Text()

		markov.Feed(phrase)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	markov.Print()

	// TODO MARKOV read parameters
	fmt.Println("I -> ", markov.GiveSuggestionList("I"))
	fmt.Println("like -> ", markov.GiveSuggestionList("like"))

	fmt.Println("I -> ", markov.GiveSuggestion("I"))
}
