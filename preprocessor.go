package main

type PreProcessor interface {
	ProcessLine(line string) string
	ProcessKey(key string) string
}
