package main

import (
	"fmt"
	"log"
	"testing"
)

func TestGiveSuggestionList(t *testing.T) {
	c := Chain{}

	c.Feed("I am Elaine")
	c.Feed("I like coding")
	c.Feed("I am a developer")
	c.Feed("I like swimming")
	c.Feed("I need coffee")

	c.Print()

	// TODO MARKOV how to handle this situation?
	words := c.GiveSuggestionList("I")
	if len(words) != 3 {
		log.Fatal("suggestions should have 3 itens: ", len(words))
	}
	if words[0] != "am" {
		log.Fatal("the first suggestion should be 'am'")
	}
	if words[1] != "like" {
		log.Fatal("the second suggestion should be 'like'")
	}
	if words[2] != "need" {
		log.Fatal("the second suggestion should be 'need'")
	}

	words = c.GiveSuggestionList("like")
	if len(words) != 2 {
		log.Fatal("suggestions should have 2 itens")
	}
	if words[0] != "coding" && words[0] != "swimming" {
		log.Fatal("the first suggestion should be 'coding' or 'swimming'")
	}
	if words[1] != "coding" && words[1] != "swimming" {
		log.Fatal("the first suggestion should be 'coding' or 'swimming'")
	}
}

func TestGiveSuggestion(t *testing.T) {
	c := Chain{}

	c.Feed("I am Elaine")
	c.Feed("I like coding")
	c.Feed("I am a developer")
	c.Feed("I like swimming")
	c.Feed("I need coffee")

	words := c.GiveSuggestion("I")

	fmt.Println("words: ", words)
}

func TestWriteText(t *testing.T) {

	c := Chain{}

	c.Feed("Talvez, um problema central da nossa vida e não apenas dos relacionamentos românticos, é que não sabemos ficar bem sozinhos. Nunca aprendemos a sustentar nossos estados emocionais de dentro para fora, sem precisar de mil objetos externos para nos auxiliar.")
	c.Feed("Isso fica visível em qualquer relação mais próxima, especialmente naquelas em que inadvertidamente criamos co-dependencia emocional. Ou seja, relações em que seu estado emocional é facilmente e constantemente modificado pelas ações ou inações do parceiro.")
	c.Feed("Se dependemos de uma ação específica do outro, por vezes desconhecida até por nós, para sustentar estados emocionais positivos, vamos necessariamente ficar reativos. Perdemos o espaço de possibilidades e a liberdade e, ao invés de sermos dois seres livres decidindo o que construir, transformamos a relação em um jogo causal de atritos.")
	c.Feed("Ele manda uma mensagem fofa e você vai dormir com um calor no peito. Ele se esquece de uma data importante e você chora na cama. Ele irritado critica algo que você fez de errado, e surpresa com a agressão ao invés de compreensão, você devolve mais forte.")
	c.Feed("Ela elogia algo que você nunca considerou atraente, e você se sente a pessoa mais amada do mundo. Ela subitamente muda de ideia sobre ir para um evento e você não consegue pensar em mais nada pelo resto do dia. Ela de mau-humor se fecha, e você frustrado não consegue evitar de fazer o clima ficar pior ainda.")
	c.Feed("Nas amizades, quando alguém surta muito por coisas pequenas, por mensagens não respondidas na hora e pequenos atrasos, vemos o amigo como mentalmente e emocionalmente instável e procuramos ajuda-lo ou nos afastamos. Por que em namoros e casamentos isso é tão mais aceitável?")
	c.Feed("Se estamos bem e temos confiança que conseguimos nos sustentar emocionalmente, o outro pode ter um movimento inesperado, desmoronar e precisar de ajuda, se afastar, e vamos ter a lucidez para não surtar por conta disso, ou de ser arrastado pela perturbação que acomete o outro.")

	c.Print()

	fmt.Println("")
	fmt.Println("")

	fmt.Println(c.WriteText(30))
}
