package main

type Transition struct {
	word1 string
	word2 string
	count uint
}

func (t *Transition) IsTransition(w1, w2 string) bool {
	return t.word1 == w1 && t.word2 == w2
}

func (t *Transition) IncCount() {
	t.count++
}
