package main

import "strings"

type SimplePreProcessor struct {
}

func (spp SimplePreProcessor) ProcessLine(line string) string {
	beGood := strings.ToLower(line)

	r := strings.NewReplacer(",", "", ".", "")
	beGood = r.Replace(beGood)

	return beGood
}

func (spp SimplePreProcessor) ProcessKey(key string) string {
	beGood := strings.ToLower(key)

	return beGood
}
