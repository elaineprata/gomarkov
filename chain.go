package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"
)

type Chain struct {
	// TODO MARKOV lowercase
	TransitionMap map[string][]*Transition
	preProcessor  interface{}
}

const LINE_BEGIN = "[LINE_BEGIN]"
const LINE_END = "[LINE_END]"

// TODO MARKOV make the private methods lowercase
// TODO MARKOV implement transitions 1,2->3

func (c *Chain) Feed(line string) {

	beGood := line
	if c.preProcessor != nil {

		if processor, ok := c.preProcessor.(PreProcessor); ok {
			beGood = processor.ProcessLine(line)
		}

	}

	words := strings.Split(beGood, " ")

	c.AddTransition(LINE_BEGIN, words[0])
	for i := 0; i < len(words); i++ {

		if i+1 < len(words) {
			c.AddTransition(words[i], words[i+1])
		}
	}
	c.AddTransition(words[len(words)-1], LINE_END)
}

// TODO MARKOV findTransition
func (c *Chain) FindTransition(w1, w2 string) *Transition {
	tarr, ok := c.TransitionMap[w1]
	if !ok {
		return nil
	}

	for i := 0; i < len(tarr); i++ {
		t := tarr[i]

		if t.IsTransition(w1, w2) {
			return t
		}
	}

	return nil
}

func (c *Chain) AddTransition(w1, w2 string) {

	if c.TransitionMap == nil {
		c.TransitionMap = make(map[string][]*Transition)
	}

	t := c.FindTransition(w1, w2)
	if t != nil {
		// TODO
		t.IncCount()
	} else {
		t = &Transition{
			word1: w1,
			word2: w2,
			count: 1,
		}

		tarr, ok := c.TransitionMap[w1]
		if ok {
			c.TransitionMap[w1] = append(tarr, t)
		} else {
			c.TransitionMap[w1] = []*Transition{t}
		}

	}

}

func (c *Chain) GiveSuggestionList(w string) []string {

	beGood := w
	if c.preProcessor != nil {

		if processor, ok := c.preProcessor.(PreProcessor); ok {
			beGood = processor.ProcessKey(w)
		}

	}

	tarr, ok := c.TransitionMap[beGood]
	if !ok {
		return nil
	}

	// sort desc
	sort.Slice(tarr, func(i, j int) bool {
		return tarr[i].count > tarr[j].count
	})

	var starr []string
	for _, t := range tarr {
		starr = append(starr, t.word2)
	}
	return starr

}

// Pick suggestion based on the odds
func (c *Chain) GiveSuggestion(w string) string {

	beGood := w
	if c.preProcessor != nil {
		if processor, ok := c.preProcessor.(PreProcessor); ok {
			beGood = processor.ProcessKey(w)
		}
	}

	tarr, ok := c.TransitionMap[beGood]
	if !ok {
		return ""
	}

	var starr []string
	for _, t := range tarr {
		for i := 0; i < int(t.count); i++ {
			starr = append(starr, t.word2)
		}
	}

	rs := rand.NewSource(time.Now().UnixNano())
	r := rand.New(rs)
	n := r.Intn(len(starr))

	return starr[n]
}

func (c *Chain) WriteText(minwords int) string {

	var b bytes.Buffer

	next := LINE_BEGIN
	for index := 0; index < minwords; index++ {
		if next != LINE_BEGIN {
			b.WriteString(next)
			b.WriteString(" ")
		}
		next = c.GiveSuggestion(next)
		if next == LINE_END {
			b.WriteString("\n")
			next = LINE_BEGIN
		}
	}
	for next != LINE_END {
		if next != LINE_BEGIN {
			b.WriteString(next)
			b.WriteString(" ")
		}
		next = c.GiveSuggestion(next)
	}

	return b.String()
}

func (c *Chain) Print() {

	for k := range c.TransitionMap {
		tarr := c.TransitionMap[k]

		fmt.Println(k)
		for _, t := range tarr {
			fmt.Println("[", t.word1, "->", t.word2, ":", t.count, "] , ")
		}
	}

}
